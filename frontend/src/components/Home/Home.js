import React from 'react'
import { Link } from 'react-router-dom'
import "./index.css"

function Home() {
    return (
      <>
      <h1>A product catalog is a type of marketing collateral that lists essential product details that help buyers make a purchase decision. 
        These details include product features, descriptions, dimensions, price,  discount,
    merchant_name,
    quantity, availability, customer reviews, and more.<span>- pradeep katheti</span></h1>

<button class="btn-open">
  <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
   <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
   <line x1="4" y1="6" x2="20" y2="6"></line>
   <line x1="4" y1="12" x2="20" y2="12"></line>
   <line x1="4" y1="18" x2="20" y2="18"></line>
</svg>
</button>
<ul class="open">
  <Link to = "/create">
  <li><span></span>Create </li></Link>
  <Link to = "/update">
  <li><span></span>Update </li></Link>
  <Link to = "/delete">
  <li><span></span> Delete</li></Link>
  
<Link to = "/get">
  <li><span></span> Get All</li></Link>
  
</ul>
      </>
    )
}

export default Home
