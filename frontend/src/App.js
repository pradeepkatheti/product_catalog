import "./App.css";
import From from "./components/From/From";
import { Container } from "@material-ui/core";
//import Retrivedata from "./components/Retrivedata/Retrivedata";
import Retrivedata from "./components/Retrivedata - Copy/Retrivedata";
import Delete from "./components/Delete/Delete";
import Update from "./components/Update/Update";
import Home from "./components/Home/Home";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import Working from "./components/Working/Working";
import Drawers from "./components/Drawer/Drawer";

function App() {
  return (
    <>
      <Container>
        <BrowserRouter>
          <Route path="/" exact component={Home} />
          <Switch>
            <Route path="/create" exact component={From} />
            <Route path="/update" exact component={Update} />
            <Route path="/delete" exact component={Delete} />
            <Route path="/get" exact component={Drawers} />

            <Route path="/component" exact component={Retrivedata} />

            {/* <Route path= "/get"  exact component = {Retrivedata}/> */}
          </Switch>
        </BrowserRouter>
      </Container>
    </>
  );
}

export default App;
