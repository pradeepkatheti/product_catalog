import React, { useState } from "react";
import "./index.css"

function Retrivedata() {
  const [user, setUser] = useState([]);
  const getttingProducts = async (event) => {
    event.preventDefault();

    const url = "http://localhost:3001/getproducts";
    const options = {
      method: "GET",

      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    const response = await fetch(url, options);
    const data = await response.json();
    setUser(data);
    if (data.status_code === 200) {
      console.log(data);
    }
  };
  console.log(user);

  return (
    <>
      <p className="h1 text-center" color="aqua">
        Getting all Products
      </p>
      <a tabindex="0" class="btn btn-lg btn-danger" role="button"  onClick={getttingProducts} data-bs-toggle="popover" data-bs-trigger="focus" title="Dismissible popover" 
      data-bs-content="And here's some amazing content. It's very engaging. Right?">Show me All</a>
     
      <div>
        {user.map((item) => {
          return (
            <div className="card mb-5"> 
            <div className="container ">
              <div className="col">
                <div className="row">
                  {" "}
                  <p className="para">Product Name  : {item.product_name}</p>
                  <p className="para">Product Description  : {item.product_description}</p>
                  <p className="para">Product Price  : {item.product_price}</p>

                  <p className="para">Product Discount  : {item.discount}</p>

                  <p className="para">Merchant Name  : {item.merchant_name}</p>

                  <p className="para">Quantity : {item.quantity}</p>




                </div>
              </div>
            </div></div>
          );
        })}
      </div>
    </>
  );
}

export default Retrivedata;
