import React, { useState } from 'react'

function Update() {

const[product_name , setProduct_name] = useState()
const [new_product_name, setNew_product_name] = useState()

    
const  updatingProductPrice = async (event) => {
    event.preventDefault()
    const user = {
      product_name,
      new_product_price : new_product_name
    }
  
    const url = "http://localhost:3001/update"
    const options = {
      method : "PUT",
      body: JSON.stringify(user),
      headers : {
        "Content-Type": "application/json",
            "Accept": "application/json",
      }
    }
  
    const response = await fetch(url,options)
    const data = await response.json()
    
    if (data.status_code === 200) {
      setProduct_name("")
      setNew_product_name("")
      

    }
  
  }
  


    return (
        <>
            <form>
        <p class="h1 text-center" color='aqua'>Updating a product catalog price</p>
        <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">give your Product Name  </label>
    <input type="text"  value={  product_name } onChange= {(event) => setProduct_name(event.target.value)} placeholder =" product name" class="form-control" id="exampleInputPassword1"/>
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Update your Price  </label>
    <input type="number"  value={  new_product_name } onChange= {(event) => setNew_product_name(event.target.value)} placeholder ="Updated product Price" class="form-control" id="exampleInputPassword1"/>
  </div>

  <button type="submit" onClick={updatingProductPrice}  class="btn btn-primary">Update</button>
        </form>
        
        </>
    )
}

export default Update
