import React, { useState } from 'react';
//import Retrivedata from '../Retrivedata/Retrivedata';
import "./index.css"


function From() {

const [product_name, setProduct_name] = useState()
const [product_description, setProduct_description] = useState()
const [ product_price, setProduct_price] = useState()
const [ discount , setDiscount] = useState()
const [ merchant_name, setMerchant_name] = useState()
const [ quantity, setQuantity] = useState()



const  submitFrom = async (event) => {
  event.preventDefault()
  const user = {
    product_name,
    product_description,
    product_price,
    discount,
    merchant_name,
    quantity
  }

  const url = "http://localhost:3001/login"
  const options = {
    method : "POST",
    body: JSON.stringify(user),
    headers : {
      "Content-Type": "application/json",
          Accept: "application/json",
    }
  }

  const response = await fetch(url,options)
  const data = await response.json()
  console.log(data)
  if (data.status_code === 200) {
    setProduct_name("")
    setProduct_description("")
    setProduct_price("")
    setDiscount("")
    setMerchant_name("")
    setQuantity("")
  }

}

    return (
        <>
        <form>

        <p class="h1 text-center heading" >Create a product catalog</p>
  
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Product Name</label>
    <input type="text"  value={product_name} onChange= {(event) => setProduct_name(event.target.value)} placeholder ="product_name" class="form-control" id="exampleInputPassword1"/>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Product Description</label>
    <input type="text"  value={ product_description} onChange= {(event) => setProduct_description(event.target.value)} placeholder ="product_description" class="form-control" id="exampleInputPassword1"/>
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Product Price</label>
    <input type="number"  value={ product_price} onChange= {(event) => setProduct_price(event.target.value)} placeholder ="Product Price" class="form-control" id="exampleInputPassword1"/>
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label"> Discount</label>
    <input type="number"  value={  discount } onChange= {(event) => setDiscount(event.target.value)} placeholder ="discount" class="form-control" id="exampleInputPassword1"/>
  </div>


  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label"> Merchant Name</label>
    <input type="text"  value={  merchant_name } onChange= {(event) => setMerchant_name(event.target.value)} placeholder ="merchant name" class="form-control" id="exampleInputPassword1"/>
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label"> Quantity </label>
    <input type="number"  value={  quantity } onChange= {(event) => setQuantity(event.target.value)} placeholder ="quantity" class="form-control" id="exampleInputPassword1"/>
  </div>

  <button type="submit" onClick={submitFrom} class="btn btn-primary">Submit</button>
  
</form>

<br/>

      
        </>
    )
}

export default From
