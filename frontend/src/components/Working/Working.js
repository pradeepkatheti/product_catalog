import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import DeleteSweepSharpIcon from '@material-ui/icons/DeleteSweepSharp';




const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
      color: "white",
      height:50,
      lineColor : "white"
      
    },
    textColor : {
      color : "white",
      fontSize: 20
    }
  }));



function Working() {
    const classes = useStyles();
    return (
        <>
        
        <div>
        <p className="h1 text-center" color="aqua">
        Getting all Products
      </p>
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="input-with-icon-adornment" className={classes.textColor}>Give the product Name</InputLabel>
        <Input className={classes.margin}
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start" className={classes.margin} >
              <DeleteSweepSharpIcon className={classes.margin}/>
            </InputAdornment>
          }
        />
      </FormControl>
    
    </div>
        </>
    )
}

export default Working


