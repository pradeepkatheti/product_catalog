import React, { useState } from "react";
import "./index.css";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setProducts } from "../../redux/actions/productActions";

function Retrivedata() {
  const dispatch = useDispatch();
  const databj = useSelector((state) => state.allProducts.products);
  console.log("all Products: ", databj);

  const clearProducts = () => {
    localStorage.removeItem("persist:Storage");
    document.location.reload(true);
  };
  
  const [user, setUser] = useState([]);

  const getttingProducts = async (event) => {
    event.preventDefault();

    const url = "http://localhost:3001/getproducts";
    const options = {
      method: "GET",

      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    const response = await fetch(url, options);
    const data = await response.json();
    dispatch(setProducts(data));
    setUser(data);
  };

  useEffect(() => {
    getttingProducts();
  }, []);

  console.log("user", user);

  return (
    <>
      <p className="h1 text-center" color="aqua">
        Getting all Products
      </p>
      <a
        tabindex="0"
        class="btn btn-lg btn-danger"
        role="button"
        onClick={getttingProducts}
        data-bs-toggle="popover"
        data-bs-trigger="focus"
        title="Dismissible popover"
        data-bs-content="And here's some amazing content. It's very engaging. Right?"
      >
        Show me All
      </a>

      <div>
        {databj.map((item) => {
          return (
            <>
              <div className="card mb-2 mt-5">
                <div className="container ">
                  <div className="col">
                    <div className="row">
                      {" "}
                      <p className="para">Product Name : {item.product_name}</p>
                      <p className="para">
                        Product Description : {item.product_description}
                      </p>
                      <p className="para">
                        Product Price : {item.product_price}
                      </p>
                      <p className="para">Product Discount : {item.discount}</p>
                      <p className="para">
                        Merchant Name : {item.merchant_name}
                      </p>
                      <p className="para">Quantity : {item.quantity}</p>
                    </div>
                  </div>
                </div>
              </div>
            </>
          );
        })}
      </div>
      <button
        type="submit"
        onClick={clearProducts}
        class="btn  mt-5 btn-primary"
      >
        Clear Products
      </button>
    </>
  );
}

export default Retrivedata;
