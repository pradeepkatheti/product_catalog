import mongoose from "mongoose"

const logindata_shema = new mongoose.Schema({
    product_name: {type:String, unique:true},
    product_description:String,
    product_price: Number,
    discount: Number,
    merchant_name: String,
    quantity:Number
}) 

export {logindata_shema}