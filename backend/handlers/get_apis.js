import { Login_data } from "../Utils/model.js";

/**
 * fetch product data details
 * @param {*} request is the request by the product
 * @param {*} response is the response with respect to the product
 */
 const get_the_products = (request, response) => {
    
    Login_data.find()
      .then((data) => {
        response.send(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  export {get_the_products} 