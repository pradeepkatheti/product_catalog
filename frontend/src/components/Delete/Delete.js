import React, { useState } from 'react'

function Delete() {
    const [product_name, setProduct_name] = useState()
    
    
const  deletingProduct = async (event) => {
    event.preventDefault()
    const user = {
      product_name
    }
  
    const url = "http://localhost:3001/delete"
    const options = {
      method : "DELETE",
      body: JSON.stringify(user),
      headers : {
        "Content-Type": "application/json",
            Accept: "application/json",
      }
    }
  
    const response = await fetch(url,options)
    const data = await response.json()
    console.log(data)
    if (data.status_code === 200) {
      setProduct_name("")

    }
  
  }
  
    return (
        <>

        <form>
        <p class="h1 text-center" color='aqua'>Deleting a product catalog</p>
        <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">give your Product Name </label>
    <input type="text"  value={  product_name } onChange= {(event) => setProduct_name(event.target.value)} placeholder ="Deleting the Product by product name" class="form-control" id="exampleInputPassword1"/>
  </div>

  <button type="submit" onClick={deletingProduct}  class="btn btn-primary">Delete</button>
        </form>
        </>
    )
}

export default Delete
