import React from "react";

import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import DeleteSweepSharpIcon from "@material-ui/icons/DeleteSweepSharp";
import AddCircleSharpIcon from "@material-ui/icons/AddCircleSharp";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import FormatListBulletedSharpIcon from "@material-ui/icons/FormatListBulletedSharp";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
  backGroubd: {
    borderColor: "white",
    color: "white",
    borderStyle: "solid",
    borderWidth: 1,
    marginTop: 30,
  },
  editt: {
    textAlign: "center",
  },
});

function Drawers() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <Link to="/component">
          {" "}
          {["Get All Product"].map((text, index) => (
            <ListItem className={classes.editt} button key={text}>
              <ListItemIcon>
                {index % 4 === 0 ? (
                  <FormatListBulletedSharpIcon />
                ) : (
                  <EditRoundedIcon />
                )}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </Link>
      </List>
    </div>
  );

  return (
    <>
      <div>
        <h1>
          A product catalog is a type of marketing collateral that lists
          essential product details that help buyers make a purchase decision.
          These details include product features, descriptions, dimensions,
          price, discount, merchant_name, quantity, availability, customer
          reviews, and more.<span>- pradeep katheti</span>
        </h1>
        {["right"].map((anchor) => (
          <React.Fragment key={anchor}>
            <Button
              className={classes.backGroubd}
              onClick={toggleDrawer(anchor, true)}
            >
              Click Me
            </Button>
            <Drawer
              anchor={anchor}
              open={state[anchor]}
              onClose={toggleDrawer(anchor, false)}
            >
              {list(anchor)}
            </Drawer>
          </React.Fragment>
        ))}
      </div>
    </>
  );
}

export default Drawers;
