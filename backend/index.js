// importing the modules
import express, { application } from "express";
import cors from "cors";
import mongoose from "mongoose";
import dotenv from "dotenv";
import bodyParser from "body-parser";
dotenv.config();
import { loginuser_handler } from "./handlers/post_apis.js";
import  {get_the_products}  from "./handlers/get_apis.js";
import { deleting_product } from "./handlers/delete.js";
import { update_product_price } from "./handlers/update.js";

//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

// frontend and backend connection by using cors
app.use(cors({ origin: "*" }));


app.post("/login",loginuser_handler)
app.get("/getproducts", get_the_products)
app.delete("/delete", deleting_product)
app.put("/update", update_product_price)


// connecting to the database
// this server is listening the port localhost 8000
//connecting the database
mongoose.connect(process.env.MONGOOSE_URL)
  .then(() => {
    console.log("Database connected!");
    app.listen(process.env.BACKENDPORT_NUMBER, () => {
      console.log(`server is running at port  ${process.env.BACKENDPORT_NUMBER}!`);
    })
  })
  .catch(() => {
    console.log("Unable to connect to database");
  })
