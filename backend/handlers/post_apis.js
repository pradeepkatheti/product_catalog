import { Login_data } from "../Utils/model.js";

const loginuser_handler = (request, response) => {
  const product_name = request.body.product_name;
  const product_description = request.body.product_description;
  const product_price = request.body.product_price;
  const discount = request.body.discount;
  const merchant_name = request.body.merchant_name;
  const quantity = request.body.quantity;
  const dataa = {
    product_name: product_name,
    product_description: product_description,
    product_price: product_price,
    discount: discount,
    merchant_name: merchant_name,
    quantity: quantity,
  };

  const posts_data = new Login_data(dataa);
  posts_data.save().then(() => {
    response.send(
      JSON.stringify({
        status_code: 200,
        status_message: "DATA added",
      })
    );
  });

  
};

export { loginuser_handler };
