import { Login_data } from "../Utils/model.js";

const deleting_product = (request, response) => {

    const product_name = request.body.product_name
  Login_data.findOneAndRemove({ product_name: product_name }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      response.send(JSON.stringify({
        status_code: 200,
        status_message: "DATA deleted",
      }));
    }
  });
};

export {deleting_product}
